export interface IHeader {
    'Content-Type': string;
    APIKey: string;
    Authorization?: string;
}
