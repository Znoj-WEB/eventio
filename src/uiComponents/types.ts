import { PathNameType } from '../types';

export interface ICloseButton {
    linkTo?: PathNameType;
    mini?: boolean;
    onClick?: () => void;
}

export interface IPlusButton {
    linkTo?: PathNameType;
    onClick?: () => void;
}

export interface ITab {
    text: JSX.Element | string;
    id: number;
    selected?: boolean;
    variant?: ButtonVariant;
    onClick?: (id: number) => void;
}

export interface IDropDown {
    items: JSX.Element[] | string[];
    title?: string;
    label?: JSX.Element | 'string';
    variant?: DropDownVariant;
    // order is a key
    onSelected?: (key: number) => void;
}

export declare type ButtonVariant =
    | 'link'
    | 'dark'
    | 'join'
    | 'leave'
    | 'edit'
    | 'plus'
    | 'switch'
    | 'tab'
    | 'close';

export declare type DropDownVariant = 'filter' | 'profie';
export interface IButton {
    text: string | JSX.Element;
    type?: 'submit' | 'reset' | 'button';
    variant?: ButtonVariant;
    loading?: boolean;
    disabled?: boolean;
    selected?: boolean;
    linkTo?: PathNameType;
    onClick?: () => void;
}

export interface IAttendButton {
    id: string;
    text?: string;
    disabled?: boolean;
    onClick?: () => void;
}

export interface IInput {
    label?: string;
    type: 'password' | 'text';
    name: string;
    value: string;
    validation?: {
        email?: boolean;
        notEmpty?: boolean;
        number?: boolean;
        futureDate?: boolean;
        date?: boolean;
        time?: boolean;
    };
    passwordEye?: boolean;
    disabled?: boolean;
    errorStyle?: boolean;
    customErrorMessage?: string;
    onChange?: (event: React.ChangeEvent<HTMLInputElement>) => void;
    onChangeToState?: (newValue: string) => void;
    onBlur?: (event: React.FocusEvent<HTMLInputElement>) => void;
    onBlurToState?: (newValue: string) => void;
    validHandler?: (valid: boolean) => void;
    validHandlerToState?: (newValue: boolean) => void;
}

export interface IValidation {
    value: string;
    setErrors?: boolean;
}

export interface ITabPanel {
    tabs: JSX.Element[] | string[];
    variant?: ButtonVariant;
    // order is a key
    onSelected?: (key: number) => void;
}
