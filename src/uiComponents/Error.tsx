import React from 'react';

import strings from '../strings';

function Error() {
    return <React.Fragment>{strings.GENERAL_ERROR}</React.Fragment>;
}

export default Error;
